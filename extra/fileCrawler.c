#define _DEFAULT_SOURCE 1	/* enables macros to test type of directory entry */
#define _TIMER 1
#include <sys/types.h>
#include <dirent.h>
#include <regex.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "tslinkedlist.h"
#include "linkedlist.h"
#include "tstreeset.h"
#include "treeset.h"
#include "re.h"

int _THREAD_NUM;
int _NUM_WORKING_THREADS;
int _DONE_CRWALING = 0;

pthread_mutex_t _MUTEX = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t _WORKERCOND = PTHREAD_COND_INITIALIZER;

typedef struct ta TA;

struct ta {
    TSTreeSet * tsts;
    TSLinkedList * tsll;
    RegExp * reg;
};
/*
 * routine to convert bash pattern to regex pattern
 *
 * e.g. if bashpat is "*.c", pattern generated is "^.*\.c$"
 *      if bashpat is "a.*", pattern generated is "^a\..*$"
 *
 * i.e. '*' is converted to ".*"
 *      '.' is converted to "\."
 *      '?' is converted to "."
 *      '^' is put at the beginning of the regex pattern
 *      '$' is put at the end of the regex pattern
 *
 * assumes 'pattern' is large enough to hold the regular expression
 */
static void cvtPattern(char pattern[],
    const char * bashpat) {
    char * p = pattern;

    * p++ = '^';
    while ( * bashpat != '\0') {
        switch ( * bashpat) {
        case '*':
            * p++ = '.'; * p++ = '*';
            break;
        case '.':
            * p++ = '\\'; * p++ = '.';
            break;
        case '?':
            * p++ = '.';
            break;
        default:
            * p++ = * bashpat;
        }
        bashpat++;
    } * p++ = '$'; * p = '\0';
}

/*
 * recursively opens directory files
 *
 * if the directory is successfully opened, it is added to the linked list
 *
 * for each entry in the directory, if it is itself a directory,
 * processDirectory is recursively invoked on the fully qualified name
 *
 * if there is an error opening the directory, an error message is
 * printed on stderr, and 1 is returned, as this is most likely indicative
 * of a protection violation
 *
 * if there is an error duplicating the directory name, or adding it to
 * the linked list, an error message is printed on stderr and 0 is returned
 *
 * if no problems, returns 1
 */

static int processDirectory(char
    const * dirname, TSLinkedList * tsll, int verbose) {
    DIR * dd;
    /*struct dirent *dent;*/
    char * sp;
    int len, status = 1;
    char d[4096];
    /*
     * eliminate trailing slash, if there
     */
    strcpy(d, dirname);
    len = strlen(d);
    if (len > 1 && d[len - 1] == '/')
        d[len - 1] = '\0';
    /* open the directory */
    if ((dd = opendir(d)) == NULL) {
        if (verbose)
            fprintf(stderr, "Error opening directory `%s'\n", d);
        return 1;
    }
    /*
     * duplicate directory name to insert into linked list
     */
    sp = strdup(d);
    if (sp == NULL) {
        fprintf(stderr, "Error adding `%s' to linked list\n", d);
        status = 0;
        goto cleanup;
    }
    if (!tsll_add(tsll, sp)) {
        fprintf(stderr, "Error adding `%s' to linked list\n", sp);
        free(sp);
        status = 0;
        goto cleanup;
    }
    if (len == 1 && d[0] == '/')
        d[0] = '\0';

    pthread_mutex_lock( &_MUTEX);
    pthread_cond_signal( & _WORKERCOND );
    pthread_mutex_unlock( &_MUTEX);

    cleanup:
        (void) closedir(dd);
    return status;
}

/*
 * comparison function between strings
 *
 * need this shim function to match the signature in treeset.h
 */
static int scmp(void * a, void * b) {
    return strcmp((char * ) a, (char * ) b);
}

/*
 * applies regular expression pattern to contents of the directory
 *
 * for entries that match, the fully qualified pathname is inserted into
 * the treeset
 */
static int applyRe(char * dir, RegExp * reg, TSTreeSet * tsts, TSLinkedList * tsll) {
    DIR * dd;
    struct dirent * dent;
    int status = 1;

    /*
     * open the directory
     */
    if ((dd = opendir(dir)) == NULL) {
        fprintf(stderr, "Error opening directory `%s'\n", dir);
        return 0;
    }
    /*
     * for each entry in the directory
     */
    while (status && (dent = readdir(dd)) != NULL) {
        if (strcmp(".", dent->d_name) == 0 || strcmp("..", dent->d_name) == 0)
            continue;
        if (!(dent->d_type & DT_DIR)) {
            char b[4096], * sp;
            /*
             * see if filename matches regular expression
             */
            if (!re_match(reg, dent->d_name))
                continue;
            sprintf(b, "%s/%s", dir, dent->d_name);

            /*
             * duplicate fully qualified pathname for insertion into treeset
             */
            if ((sp = strdup(b)) != NULL) {
                if (!tsts_add(tsts, sp)) {
                    fprintf(stderr, "Error adding `%s' to tree set\n", sp);
                    free(sp);
                    status = 0;
                    break;
                }
            } else {
                fprintf(stderr, "Error adding `%s' to tree set\n", b);
                status = 0;
                break;
            }
        } else {
            /* if it is not a file then we write the directory in the linked list*/
                char b[4096];
		sprintf(b, "%s/%s", dir, dent->d_name);
		processDirectory(b, tsll, 0);
	    }
    }
    (void) closedir(dd);
    return status;
}

static void decreWorkingThreadNum() {
    pthread_mutex_lock( &_MUTEX);
    _NUM_WORKING_THREADS--;
    pthread_mutex_unlock( &_MUTEX);
}

static void increWorkingThreadNum() {
    pthread_mutex_lock( &_MUTEX);
    _NUM_WORKING_THREADS++;
    pthread_mutex_unlock( &_MUTEX);
}

static int noOneWorking() {
    int result;
    pthread_mutex_lock( &_MUTEX);
    result = _NUM_WORKING_THREADS;
    pthread_mutex_unlock( &_MUTEX);
    return (result == 0);
}

static void setDONE() {
    pthread_mutex_lock( &_MUTEX);
    _DONE_CRWALING = 1;
    pthread_mutex_unlock( &_MUTEX);
}

void * worker(void * arg) {
    TA * ta = (TA * ) arg;
    char * sp;
    while (!_DONE_CRWALING) {
        if (tsll_removeFirst(ta->tsll, (void * * ) & sp)) {
            int stat = applyRe(sp, ta->reg, ta->tsts, ta->tsll);
	    free(sp);
            if (!stat) {
                printf("THREAD ***** Error processing directory%s\n", sp);
                break;
            }
        } else {

            /* if we find out the queue is empty we put this calling thread to sleep */
            decreWorkingThreadNum();

            if (noOneWorking()) {
                setDONE();
                break;
            }

	    pthread_mutex_lock(&_MUTEX);
            while (tsll_size(ta->tsll) == 0 && !_DONE_CRWALING)
                pthread_cond_wait( &_WORKERCOND, &_MUTEX);
            pthread_mutex_unlock( &_MUTEX );

            increWorkingThreadNum();
        }
    }

    pthread_cond_broadcast( &_WORKERCOND );
    return NULL;
}

static int createWorkerArgs(TA ** ta, TSTreeSet * tsts, TSLinkedList * tsll, RegExp * reg) {
    int status = 0;
    *ta = (TA * ) malloc(sizeof(TA));
    if ( * ta != NULL) {
        ( * ta)->tsts = tsts;
        ( * ta)->tsll = tsll;
        ( * ta)->reg = reg;
        status = 1;
    } else {
        free( * ta);
    }

    return status;
}

int main(int argc, char * argv[]) {
    char * envval;
    clock_t begin_time, end_time;
    double time_s;

    TSLinkedList * tsll         = NULL;
    TSTreeSet * tsts            = NULL;
    TA * threadArg              = NULL;
    pthread_t * pids;
    char pattern[4096];
    RegExp * reg;
    TSIterator * tsit;
    int i;

    /* timmer begin */

    begin_time = clock();

    if (argc < 2) {
        fprintf(stderr, "Usage: ./fileCrawler pattern [dir] ...\n");
        return -1;
    }

    /* convert bash expression to regular expression and compile */
    cvtPattern(pattern, argv[1]);
    if ((reg = re_create()) == NULL) {
        fprintf(stderr, "Error creating Regular Expression Instance\n");
        return -1;
    }

    if (!re_compile(reg, pattern)) {
        char eb[4096];
        re_status(reg, eb, sizeof eb);
        fprintf(stderr, "Compile error - pattern: `%s', error message: `%s'\n",
            pattern, eb);
        re_destroy(reg);
        return -1;
    }

    /* create linked list and treeset */
    if ((tsll = tsll_create()) == NULL) {
        fprintf(stderr, "Unable to create linked list\n");
        goto done;
    }

    if ((tsts = tsts_create(scmp)) == NULL) {
        fprintf(stderr, "Unable to create tree set\n");
        goto done;
    }

    /* populate linked list */
    if (argc == 2) {
        if (!processDirectory(".", tsll, 1))
            goto done;
    } 

    else {
        int i;
        for (i = 2; i < argc; i++) {
            if (!processDirectory(argv[i], tsll, 1))
	    goto done;
        }
    }

    /*==================================================================*/

    /* get thread number by reading from environment variable */
    if ((envval = getenv("CRAWLER_THREADS")) == NULL){
        printf("Environment variable 'CRAWLER_THREADS' is not set\n");
        goto done; 
    }

    _THREAD_NUM = _NUM_WORKING_THREADS = (int)( *envval - '0');

    /* get thread number by reading from environment variable */
    pids = (pthread_t *) malloc( _THREAD_NUM * sizeof(pthread_t));

    if (pids == NULL) {
        goto done;
    }

    if (!createWorkerArgs( & threadArg, tsts, tsll, reg)) {
        goto done;
    }


    /* create all the threads */
    for (i = 0; i < _THREAD_NUM; i++) {
	pthread_create( (pids + i), NULL, &worker, threadArg);
    }

    /* blocking function call wait for threads to terminate */
    for (i = 0; i < _THREAD_NUM; i++) {
	pthread_join( *(pids + i), NULL);
    }

    /*  create iterator to traverse files matching pattern in sorted order */
    if ((tsit = tsts_it_create(tsts)) == NULL) {
        fprintf(stderr, "Unable to create iterator over tree set\n");
        goto done;
    }

    while (tsit_hasNext(tsit)) {
        char * s;
        (void) tsit_next(tsit, (void * * ) & s);
        printf("%s\n", s);
    }
    
    tsit_destroy(tsit);
    pthread_cond_broadcast( &_WORKERCOND);

    /* cleanup after ourselves so there are no memory leaks */
    done:
        if (tsll != NULL)
            tsll_destroy(tsll, free);
        if (tsts != NULL)
            tsts_destroy(tsts, free);

	if (envval != NULL)
            free(pids); 

        re_destroy(reg);
        free(threadArg);
        pthread_mutex_destroy( &_MUTEX);
        pthread_cond_destroy( &_WORKERCOND);

    end_time = clock();
    time_s = (double)( end_time - begin_time)/ CLOCKS_PER_SEC;

    if (_TIMER)
    	printf("Totally time spent in this program is %f\n", time_s);
    return 0;

}
