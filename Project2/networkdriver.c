/*
	Author Rui Tu
*/
#include "./include/BoundedBuffer.h"
#include "./include/diagnostics.h"
#include "./include/freepacketdescriptorstore__full.h"
#include "./include/freepacketdescriptorstore.h"
#include "./include/networkdevice.h"
#include "./include/networkdriver.h"
#include "./include/packetdescriptor.h"
#include "./include/packetdescriptorcreator.h"
#include "./include/pid.h"
#include <pthread.h>
#include <unistd.h>

#define _APP_BUFF_SIZE 4
#define _DEVICE_BUFF_SIZE 36
#define MAX_PID 10

BoundedBuffer              * _ND_BUFF;
BoundedBuffer              * _APP_BUFF_LIST[ MAX_PID + 1 ];
FreePacketDescriptorStore  * _FPDS_PTR;
NetworkDevice              * _ND_PTR;


/* thread for sending packets */
void * send_to_networkdevice( __attribute__((unused)) void * arg ){	
	PacketDescriptor * pd_ptr = NULL;
	while (1) {
		if ( nonblockingReadBB( _ND_BUFF, (void **) &pd_ptr ) ) {
			send_packet( _ND_PTR, pd_ptr );
			nonblocking_put_pd( _FPDS_PTR, pd_ptr );
		}
	}
}

/* thread for receiving packets */
void * receiving_from_networkdevice( __attribute__((unused)) void * arg ){
	PacketDescriptor * pd = NULL; 
	PacketDescriptor * backup_pd = NULL;

	/* create one first */
	blocking_get_pd( _FPDS_PTR, &pd );
	init_packet_descriptor( pd );

	while (1) {

		/* if nonblocking we wrtie */
		if ( nonblocking_get_pd( _FPDS_PTR, &backup_pd ) ) {
			
			init_packet_descriptor( pd );
			register_receiving_packetdescriptor( _ND_PTR, pd );
			await_incoming_packet( _ND_PTR );
			
			/* if fail to write then we resure the it. */
			if ( !nonblockingWriteBB( _APP_BUFF_LIST[ packet_descriptor_get_pid(pd) % MAX_PID], pd ))
				nonblocking_put_pd( _FPDS_PTR, pd);

			pd = backup_pd;
		
		/* else we reuse */
		} else {
			init_packet_descriptor( backup_pd );
			register_receiving_packetdescriptor( _ND_PTR, backup_pd );
			await_incoming_packet( _ND_PTR );
			nonblocking_put_pd( _FPDS_PTR, backup_pd);
		}
	}
}

void init_network_driver(NetworkDevice               *nd,
                         void                        *mem_start,
                         unsigned long               mem_length,
                         FreePacketDescriptorStore **fpds_ptr) {

	pthread_t send_thread, receive_thread;
	int i;

	/* create netword device buffer */
  	_ND_BUFF  = createBB(_DEVICE_BUFF_SIZE);

  	/* create application buffers */
	for (i = 0; i < MAX_PID + 1; i++) { _APP_BUFF_LIST[i] = createBB(_APP_BUFF_SIZE); }
	_FPDS_PTR = create_fpds();
	_ND_PTR    = nd;

 	/* create store */
	create_free_packet_descriptors( _FPDS_PTR, mem_start, mem_length );
	*fpds_ptr = _FPDS_PTR;

  	/* create application buffers */
	if(pthread_create(&send_thread, NULL, send_to_networkdevice, NULL)) 	      { fprintf(stderr, "Error creating send thread\n"); }
	if(pthread_create(&receive_thread, NULL, receiving_from_networkdevice, NULL)) { fprintf(stderr, "Error creating receive_thread thread\n"); }
}

void blocking_send_packet(PacketDescriptor * pd) 			{ blockingWriteBB( _ND_BUFF, pd ); }
int  nonblocking_send_packet(PacketDescriptor *pd) 			{ return ( nonblockingWriteBB( _ND_BUFF, pd) == 1 ); }
void blocking_get_packet(PacketDescriptor **pd, PID pid) 	{ * pd = blockingReadBB( _APP_BUFF_LIST[ pid % MAX_PID ] ); }
int  nonblocking_get_packet(PacketDescriptor **pd, PID pid) { return nonblockingReadBB( _APP_BUFF_LIST[ pid % MAX_PID ], (void **)pd ); }