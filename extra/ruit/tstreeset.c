#include "treeset.h"
#include "tstreeset.h"
#include <pthread.h>
#include <stdlib.h>

#define LOCK(tsts) &((tsts)->lock)

struct tstreeset {
    TreeSet * ts;
    pthread_mutex_t lock;
};

/*
 * create a tststreeset that is ordered using `cmpFunction' to compare two elementsts
 *
 * returns a pointer to the tststreeset, or NULL if there are malloc() errors
 */
TSTreeSet *tsts_create(int (*cmpFunction)(void *, void *)) {
    TSTreeSet * tsts = (TSTreeSet *) malloc (sizeof (TSTreeSet));
    if (tsts != NULL) {
        TreeSet * ts = ts_create( cmpFunction );
        if (ts == NULL) {
            free(ts);
            free(tsts);
        } else {
            pthread_mutexattr_t ma;
            pthread_mutexattr_init(&ma);
            pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
            tsts->ts = ts;
            pthread_mutex_init(LOCK(tsts), &ma);
            pthread_mutexattr_destroy(&ma);
        }
    } else {
        free(tsts);
    }

    return tsts;
}

/*
 * destroys the tststreeset; for each element, if userFunction != NULL,
 * it is invoked on that element; the storage associated with
 * the tststreeset is then returned to the heap
 */
void tsts_destroy(TSTreeSet *tsts, void (*userFunction)(void *element)) {
    pthread_mutex_lock(LOCK(tsts));
    ts_destroy(tsts->ts, userFunction);
    pthread_mutex_unlock(LOCK(tsts));
    pthread_mutex_destroy(LOCK(tsts));
    free(tsts);
}

/*
 * adds the specified element to the set if it is not already present
 *
 * returns 1 if the element was added, 0 if the element was already present
 */
int tsts_add(TSTreeSet *tsts, void *element) {
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_add(tsts->ts, element);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * returns the least element in the set greater than or equal to `element'
 *
 * returns 1 if found, or 0 if no such element
 */
int tsts_ceiling(TSTreeSet *tsts, void *element, void **ceiling) {
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_ceiling(tsts->ts, element, ceiling);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * clears all elementsts from the tststreeset; for each element,
 * if userFunction != NULL, it is invoked on that element;
 * any storage associated with that element in the tststreeset is then
 * returned to the heap
 *
 * upon return, the tststreeset will be empty
 */
void tsts_clear(TSTreeSet *tsts, void (*userFunction)(void *element)){
    pthread_mutex_lock(LOCK(tsts));
    ts_clear(tsts->ts, userFunction);
    pthread_mutex_unlock(LOCK(tsts));
}

/*
 * returns 1 if the set contains the specified element, 0 if not
 */
int tsts_contains(TSTreeSet *tsts, void *element) {
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_contains(tsts->ts, element);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * returns the first (lowest) element currently in the set
 *
 * returns 1 if non-empty, 0 if empty
 */
int tsts_first(TSTreeSet *tsts, void **element){
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_first(tsts->ts, element);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}


/*
 * returns the greatest element in the set less than or equal to `element'
 *
 * returns 1 if found, or 0 if no such element
 */
int tsts_floor(TSTreeSet *tsts, void *element, void **floor) {
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_floor(tsts->ts, element, floor);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * returns the least element in the set strictly greater than `element'
 *
 * returns 1 if found, or 0 if no such element
 */
int tsts_higher(TSTreeSet *tsts, void *element, void **higher) {
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_higher(tsts->ts, element, higher);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * returns 1 if the set contains no elementsts, 0 otherwise
 */
int tsts_isEmpty(TSTreeSet *tsts)  {
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_isEmpty(tsts->ts);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}


/*
 * returns the last (highest) element currently in the set
 *
 * returns 1 if non-empty, 0 if empty
 */
int tsts_last(TSTreeSet *tsts, void **element) {
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_last(tsts->ts, element);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}


/*
 * returns the greatest element in the set strictly less than `element'
 *
 * returns 1 if found, or 0 if no such element
 */
int tsts_lower(TSTreeSet *tsts, void *element, void **lower){
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_lower(tsts->ts, element, lower);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * retrieves and removes the first (lowest) element
 *
 * returns 0 if set was empty, 1 otherwise
 */
int tsts_pollFirst(TSTreeSet *tsts, void **element){
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_pollFirst(tsts->ts, element);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * retrieves and removes the last (highest) element
 *
 * returns 0 if set was empty, 1 otherwise
 */
int tsts_pollLast(TSTreeSet *tsts, void **element) {
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_pollLast(tsts->ts, element);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * removes the specified element from the set if present
 * if userFunction != NULL, invokes it on the element before removing it
 *
 * returns 1 if successful, 0 if not present
 */
int tsts_remove(TSTreeSet *tsts, void *element, void (*userFunction)(void *element)){
    int result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_remove(tsts->ts, element, userFunction);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * returns the number of elementsts in the tststreeset
 */
long tsts_size(TSTreeSet *tsts){
    long result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_size(tsts->ts);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * return the elementsts of the tststreeset as an array of void * pointers
 * the order of elementsts in the array is the as determined by the tststreeset's
 * compare function
 *
 * returns pointer to the array or NULL if error
 * returns number of elementsts in the array in len
 */
void **tsts_toArray(TSTreeSet *tsts, long *len){
    void ** result;
    pthread_mutex_lock(LOCK(tsts));
    result = ts_toArray(tsts->ts, len);
    pthread_mutex_unlock(LOCK(tsts));
    return result;
}

/*
 * create generic iterator to this tststreeset
 *
 * returns pointer to the Iterator or NULL if failure
 */
TSIterator *tsts_it_create(TSTreeSet *tsts) {
    TSIterator *it = NULL;
    void **tmp;
    long len;

    pthread_mutex_lock(LOCK(tsts));
    tmp = tsts_toArray(tsts, &len);
    if (tmp != NULL) {
        it = tsit_create(LOCK(tsts), len, tmp);
        if (it == NULL)
            free(tmp);
    }

    if (it == NULL)
        pthread_mutex_unlock(LOCK(tsts));

    return it;
}
