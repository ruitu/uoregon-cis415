#include "iterator.h"
#include "tsiterator.h"
#include "stdlib.h"
#include <pthread.h>

struct tsiterator {
   Iterator * it;
   pthread_mutex_t *lock;
};


TSIterator *tsit_create(pthread_mutex_t * lock, long size, void **elements) {
   TSIterator *tsit = (TSIterator *)malloc(sizeof(TSIterator));

    if (tsit != NULL) {
        tsit->it = it_create( size, elements );
    	tsit->lock = lock;
    	if (tsit->it == NULL) {
    	    free(tsit->it);
    	    tsit_destroy(tsit);
    	}
   } else {
       tsit_destroy(tsit);
   }

   return tsit;
}

int tsit_hasNext(TSIterator *tsit) {
    return it_hasNext(tsit->it);
}

int tsit_next(TSIterator *tsit, void **element) {
   return it_next(tsit->it, element);
}

void tsit_destroy(TSIterator *tsit) {
    pthread_mutex_lock(tsit->lock);
    it_destroy(tsit->it);
    pthread_mutex_unlock(tsit->lock);
    pthread_mutex_destroy(tsit->lock);
    free(tsit);
}
