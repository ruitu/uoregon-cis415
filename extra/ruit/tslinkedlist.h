#ifndef TSLINKEDLIST_H_
#define TSLINKEDLIST_H_

#include <stdlib.h>
#include "iterator.h"
#include "tsiterator.h"

typedef struct tslinkedlist TSLinkedList;		/* opaque type definition */
/*
 * create a linked list
 *
 * returns a pointer to the linked list, or NULL if there are malloc() errors
 */
TSLinkedList *tsll_create(void);

/*
 * destroys the linked list; for each element, if userFunction != NULL, invokes
 * userFunction on the element; then returns any list structure associated with
 * the element; finally, deletes any remaining structures associated with the
 * list
 */
void tsll_destroy(TSLinkedList *ll, void (*userFunction)(void *element));

/*
 * appends `element' to the end of the list
 *
 * returns 1 if successful, 0 if unsuccesful (malloc errors)
 */
int tsll_add(TSLinkedList *ll, void *element);

/*
 * inserts `element' at the specified position in the list;
 * all elements from `index' upwards are shifted one position;
 * if current size is N, 0 <= index <= N must be true
 *
 * returns 1 if successful, 0 if unsuccessful (malloc errors)
 */
int tsll_insert(TSLinkedList *ll, long i, void *element);

/*
 * inserts `element' at the beginning of the list
 * equivalent to ll_insert(ll, 0, element);
 */
int tsll_addFirst(TSLinkedList *ll, void *element);

/*
 * appends `element' at the end of the list
 * equivalent to ll_add(ll, element);
 */
int tsll_addLast(TSLinkedList *ll, void *element);

/*
 * clears the linked list; for each element, if userFunction != NULL, invokes
 * userFunction on the element; then returns any list structure associated with
 * the element
 *
 * upon return, the list is empty
 */
void tsll_clear(TSLinkedList *ll, void (*userFunction)(void *element));

/*
 * Retrieves, but does not remove, the element at the specified index
 *
 * return 1 if successful, 0 if not
 */
int tsll_get(TSLinkedList *tsll, long index, void **element);

/*
 * Retrieves, but does not remove, the first element
 *
 * return 1 if successful, 0 if not
 */
int tsll_getFirst(TSLinkedList *tsll, void **element);

/*
 * Retrieves, but does not remove, the last element
 *
 * return 1 if successful, 0 if not
 */
int tsll_getLast(TSLinkedList *tsll, void **element);

/*
 * Retrieves, and removes, the element at the specified index
 *
 * return 1 if successful, 0 if not
 */
int tsll_remove(TSLinkedList *tsll, long index, void **element);

/*
 * Retrieves, and removes, the first element
 *
 * return 1 if successful, 0 if not
 */
int tsll_removeFirst(TSLinkedList *tsll, void **element);

/*
 * Retrieves, and removes, the last element
 *
 * return 1 if successful, 0 if not
 */
int tsll_removeLast(TSLinkedList *tsll, void **element);

/*
 * Replaces the element at the specified index; the previous element
 * is returned in `*previous'
 *
 * return 1 if successful, 0 if not
 */
int tsll_set(TSLinkedList *tsll, long index, void *element, void **previous);

/*
 * returns the number of elements in the linked list
 */
long tsll_size(TSLinkedList *tsll);

/*
 * returns an array containing atsll of the elements of the linked list in
 * proper sequence (from first to last element); returns the length of the
 * list in `len'
 *
 * returns pointer to void * array of elements, or NULL if malloc failure
 */
void **tsll_toArray(TSLinkedList *ll, long *len);

/*
 * creates an iterator for running through the linked list
 *
 * returns pointer to the Iterator or NULL
 */
TSIterator *tsll_it_create(TSLinkedList *ll);

#endif /* _LINKEDLIST_H_ */
