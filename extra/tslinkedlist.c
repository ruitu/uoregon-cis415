#include "linkedlist.h"
#include "tslinkedlist.h"
#include "iterator.h"
#include "tsiterator.h"
#include <stdlib.h>
#include <pthread.h>

#define LOCK(tsll) &((tsll)->lock)

struct tslinkedlist {
   LinkedList * ll;
   pthread_mutex_t lock;
};


TSLinkedList *tsll_create(void) {
    TSLinkedList * tsll = (TSLinkedList *) malloc ( sizeof (TSLinkedList));
    if (tsll != NULL) {
        LinkedList * ll = ll_create();
        if (ll == NULL) {
            free(tsll);
            ll = NULL;
        } else {
            pthread_mutexattr_t ma;
            pthread_mutexattr_init(&ma);
            pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
            tsll->ll = ll;
            pthread_mutex_init(LOCK(tsll), &ma);
            pthread_mutexattr_destroy(&ma);
    	}
   } else {
       free(tsll);
   }
   return tsll;
}


void tsll_destroy(TSLinkedList *tsll, void (*userFunction)(void *element)) {
    pthread_mutex_lock(LOCK(tsll));
    ll_destroy(tsll->ll, userFunction);
    pthread_mutex_unlock(LOCK(tsll));
    pthread_mutex_destroy(LOCK(tsll));
    free(tsll);
}


int tsll_add(TSLinkedList *tsll, void *element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_add(tsll->ll, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}

/*
 * inserts `element' at the specified position in the list;
 * all elements from `index' upwards are shifted one position;
 * if current size is N, 0 <= index <= N must be true
 *
 * returns 1 if successful, 0 if unsuccessful (malloc errors)
 */
int tsll_insert(TSLinkedList *tsll, long i, void *element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_insert(tsll->ll, i, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * inserts `element' at the beginning of the list
 * equivalent to ll_insert(ll, 0, element);
 */
int tsll_addFirst(TSLinkedList *tsll, void *element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_addFirst(tsll->ll, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * appends `element' at the end of the list
 * equivalent to ll_add(ll, element);
 */
int tsll_addLast(TSLinkedList *tsll, void *element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_addLast(tsll->ll, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * clears the linked list; for each element, if userFunction != NULL, invokes
 * userFunction on the element; then returns any list structure associated with
 * the element
 *
 * upon return, the list is empty
 */
void tsll_clear(TSLinkedList *tsll, void (*userFunction)(void *element)) {
    pthread_mutex_lock(LOCK(tsll));
    ll_clear(tsll->ll, userFunction);
    pthread_mutex_unlock(LOCK(tsll));
}
/*
 * Retrieves, but does not remove, the element at the specified index
 *
 * return 1 if successful, 0 if not
 */
int tsll_get(TSLinkedList *tsll, long index, void **element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_get(tsll->ll, index, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * Retrieves, but does not remove, the first element
 *
 * return 1 if successful, 0 if not
 */
int tsll_getFirst(TSLinkedList *tsll, void **element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_getFirst(tsll->ll, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;


}
/*
 * Retrieves, but does not remove, the last element
 *
 * return 1 if successful, 0 if not
 */
int tsll_getLast(TSLinkedList *tsll, void **element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_getLast(tsll->ll, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * Retrieves, and removes, the element at the specified index
 *
 * return 1 if successful, 0 if not
 */
int tsll_remove(TSLinkedList *tsll, long index, void **element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_remove(tsll->ll, index, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * Retrieves, and removes, the first element
 *
 * return 1 if successful, 0 if not
 */
int tsll_removeFirst(TSLinkedList *tsll, void **element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_removeFirst(tsll->ll, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}

/*
 * Retrieves, and removes, the last element
 *
 * return 1 if successful, 0 if not
 */
int tsll_removeLast(TSLinkedList *tsll, void **element) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_removeLast(tsll->ll, element);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}

/*
 * Replaces the element at the specified index; the previous element
 * is returned in `*previous'
 *
 * return 1 if successful, 0 if not
 */
int tsll_set(TSLinkedList *tsll, long index, void *element, void **previous) {
    int result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_set(tsll->ll, index, element, previous);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * returns the number of elements in the linked list
 */
long tsll_size(TSLinkedList *tsll) {
    long result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_size(tsll->ll);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * returns an array containing atsll of the elements of the linked list in
 * proper sequence (from first to last element); returns the length of the
 * list in `len'
 *
 * returns pointer to void * array of elements, or NULL if malloc failure
 */
void **tsll_toArray(TSLinkedList *tsll, long *len) {
    void ** result;
    pthread_mutex_lock(LOCK(tsll));
    result = ll_toArray(tsll->ll, len);
    pthread_mutex_unlock(LOCK(tsll));
    return result;
}
/*
 * creates an iterator for running through the linked list
 *
 * returns pointer to the Iterator or NULL
 */
TSIterator * tsll_it_create(TSLinkedList *tsll) {
    TSIterator * it = NULL;
    void ** tmp;
    long len;

    pthread_mutex_lock(LOCK(tsll));
    tmp = tsll_toArray(tsll, &len);
    if (tmp != NULL) {
	it = tsit_create(LOCK(tsll), len, tmp);
	if (it == NULL)  free(tmp);

    }

    if (it == NULL) pthread_mutex_unlock(LOCK(tsll));
    return it;
}
