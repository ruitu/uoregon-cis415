#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;

struct date {
    uint8_t  day;
    uint8_t  month;
    uint16_t  year;
};

Date *date_create(char *datestr) {
    Date* new_date  = (Date*) malloc( sizeof(Date) );
    uint8_t  nd  = (*(datestr + 0) - '0') * 10 + *(datestr + 1) - '0';
    uint8_t  nm  = (*(datestr + 3) - '0') * 10 + *(datestr + 4) - '0';
    uint16_t ny  = 0;
    size_t i;

    for (i = 0; i < 4; i++) { ny += ( *(datestr + 9 - i) - '0' ) * pow(10, i); }

    if (new_date == NULL) {
        free(new_date);
    } else {
        new_date->day   = nd;
        new_date->month = nm;
        new_date->year  = ny;
    }
    return new_date;
}

Date *date_duplicate(Date *d) {
    Date *dup_date = (Date*) malloc( sizeof(Date) );
    if (dup_date == NULL) return NULL;
    memcpy(dup_date, d, sizeof((*dup_date)));
    return dup_date;
}


int date_compare(Date *date1, Date *date2) {
    int date1_val = (date1->year * 10000 + date1->month * 100 + date1->day);
    int date2_val = (date2->year * 10000 + date2->month * 100 + date2->day);
    return (date1_val - date2_val);
}


void date_destroy(Date *d) {
    free(d);
    d = NULL;
}
