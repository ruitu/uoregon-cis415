#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "p1fxns.h"

static char * p1_strncpy(char *dest, const char *src, size_t n) {
    size_t i;

   for (i = 0; i < n && src[i] != '\0'; i++)
        dest[i] = src[i];
    for ( ; i < n; i++)
        dest[i] = '\0';

   return dest;
}


// http://www.sanfoundry.com/c-program-integer-to-string-vice-versa/
void tostring(char str[], int num) {
    int i, rem, len = 0, n;
 
    n = num;
    while (n != 0)
    {
        len++;
        n /= 10;
    }
    for (i = 0; i < len; i++)
    {
        rem = num % 10;
        num = num / 10;
        str[len - (i + 1)] = rem + '0';
    }
    str[len] = '\0';
}


void print_exec_info( pid_t pid ) {
	char line_buffer[1024];
	char pid_string[1024];
	char temp_time_buffer[1024];
	char path[1024];
	int cur_pos = 0;
	int tempword_pos = 0;

	tostring(pid_string, pid);
	p1_strncpy( (path + cur_pos), "/proc/", (p1strlen("/proc/")) );
	cur_pos += p1strlen("/proc/");
	p1_strncpy( (path + cur_pos), pid_string, p1strlen(pid_string) + 1);
	cur_pos += p1strlen(pid_string);
	p1_strncpy( (path + cur_pos), "/stat", p1strlen("/stat") + 1 );
	
	int fd = open(path, O_RDONLY);
	read(fd, line_buffer, 1024);

	// printf("line_buffer: %s\n", line_buffer);
	char user_time[1024], kernel_time[1024];
	int i;
	for (i = 0; i < 52; i++) {
		tempword_pos = p1getword(line_buffer, tempword_pos, temp_time_buffer);
		if (i == 13) {
			printf("User execution time: %s\n", temp_time_buffer);
		} 

		if (i == 14) {
			printf("Kernel execution time: %s\n", temp_time_buffer);
		}
	}
}



int main(void) {
	// char line_buffer[1024],  temp_line[1024], curword_buf[1024];
	
	// int fd = open("/proc/1/stat", O_RDONLY);

	// int tempword_pos = 0;

	// read(fd, line_buffer, 1024);
	
	// p1_strncpy( temp_line, line_buffer, 1024 );

	// int i;
	// for (i = 0; i < 15; i++) {
	// 	tempword_pos = p1getword(temp_line, tempword_pos, curword_buf);
	// 	printf("curword_buf is %s\n", curword_buf);
	// }

	print_exec_info(1);
	//printf("%s\n", line_buffer);

	return 0;
}	