#ifndef _PROC_QUEUE_
#define _PROC_QUEUE_
#include <sys/wait.h>

typedef struct proc Proc;
typedef struct pqueue Pqueue;
typedef struct pqueue_iterator Pqueue_iterator;

struct pqueue_iterator {
    Proc ** procs;
    int cur_index;
    long max_index;
};

struct proc {
    pid_t pid;
    int dead;
    int args_size;
    char ** cmd;
    struct proc * next;
};


struct pqueue {
    long size;
    Proc * head;
    Proc * tail;
};


Proc * proc_create(void);


void proc_destroy( Proc * proc);

void proc_pop_destroy( Proc * proc);

Pqueue * pqueue_create(void);

void pqueue_push(Pqueue * queue,  Proc * proc );

void pqueue_next_turn(Pqueue * queue);

Proc * pqueue_pop( Pqueue * queue );

void pqueue_destroy( Pqueue * queue);

Pqueue_iterator * pqueue_iter_create(Pqueue * queue);

Proc * pqueue_iter_next( Pqueue_iterator * it );

void pqueue_iter_destroy( Pqueue_iterator * it );

#endif
