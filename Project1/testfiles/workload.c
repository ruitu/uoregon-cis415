#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
int main(int argc, char * args[]) {
    int number = atoi(args[2]);
    int id = atoi(args[1]);
    int i;
    for (i = 0; i < number; i++) {
        printf("Workload %d counting down  %d\n",id, i);
        sleep(1);    
    }
    return 0;
}
