#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "proc_queue.h"


Proc * proc_create(void) {
    Proc * new_proc = (Proc *) malloc( sizeof(Proc) );
    if (new_proc == NULL) {
        free(new_proc);
    } else {
        new_proc->pid       = -1;
        new_proc->dead      =  0;
        new_proc->args_size = -1;
        new_proc->cmd       = NULL;
        new_proc->next      = NULL;
    }
    return new_proc;
}


void proc_destroy( Proc * proc ) {
    int i;
    for (i = 0; i < proc->args_size; i++)
        free(proc->cmd[i]);

    free(proc->cmd);
    free(proc);
}


Pqueue * pqueue_create(void) {
    Pqueue * queue = ( Pqueue * ) malloc( sizeof (Pqueue) );
    if (queue == NULL) {
        free(queue);
    } else {
        queue->size = 0;
        queue->head = NULL;
        queue->tail = NULL;
    }

    return queue;
}


Pqueue_iterator * pqueue_iter_create(Pqueue * queue) {
    Proc ** procs = ( Proc ** ) malloc( queue->size * sizeof( Proc * ) );
    Pqueue_iterator * it =  ( Pqueue_iterator *) malloc ( sizeof (Pqueue_iterator) );
    if (procs == NULL || it == NULL) {
        perror("Pqueue memory allocation failed\n");
        free(procs);
        free(it);
    } else {
        it->procs = procs;
        it->cur_index = 0;
        it->max_index = queue->size - 1;
        Proc * cur_proc = queue->head;
        int count = 0;
        while ( cur_proc != 0 ) {
            *( procs + count ) = cur_proc;
            count++;
            cur_proc = cur_proc->next;
        }
    }
    return it;
}


Proc * pqueue_iter_next( Pqueue_iterator * it ) {
    if (it->cur_index == it->max_index) return NULL;
    else return *( it->procs + (it->cur_index)++ );
}


void pqueue_iter_destroy( Pqueue_iterator * it ) {
    free(it->procs);
    free(it);
}


void pqueue_push(Pqueue * queue,  Proc * proc ) {
    if (queue->size == 0) {
        queue->head = proc;
        queue->tail = proc;
    } else {
        queue->tail->next = proc;
        queue->tail       = proc;
    }

    (queue->size)++;
}

void pqueue_pop_destory( Pqueue * queue ) {
    Proc * pop_proc = pqueue_pop(queue);
    proc_destroy(pop_proc);
}


Proc * pqueue_pop( Pqueue * queue) {
    Proc * pop_proc;
    if (queue->size == 0)
        return  NULL;

    pop_proc = queue->head;

    if (queue->size == 1) queue->head = queue->tail = NULL;
    else                  queue->head = queue->head->next;

    (queue->size)--;
    pop_proc->next = NULL;
    return pop_proc;
}


void pqueue_next_turn( Pqueue * queue ) {
    //f (queue->size)
    pqueue_push( queue, pqueue_pop( queue ) );
}


void pqueue_destroy( Pqueue * queue) {
    Proc * cur_proc = queue->head;
    Proc * next_proc;
    while (cur_proc != NULL) {
        next_proc = cur_proc->next;
        proc_destroy(cur_proc);
        cur_proc = next_proc;
    }
    free(queue);
}
