#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "p1fxns.h"
#include "proc_queue.h"
#define TIME_SLICE 1

int wakeup_received = 0;
int STARTED = 0;
int NUM_CHILD = 0;
Pqueue * PROC_QUEUE;
Proc   * CUR_PROC;
Proc  ** PROC_ARRAY;

// http://linux.die.net/man/3/strncpy crabbed from here
static char * p1_strncpy(char *dest, const char *src, size_t n) {
    size_t i;

   for (i = 0; i < n && src[i] != '\0'; i++)
        dest[i] = src[i];
    for ( ; i < n; i++)
        dest[i] = '\0';

   return dest;
}


Pqueue * read_input() {
    char line_buf[100], curword_buf[100];
    int curline_index = 0, line_size = -1;

    Pqueue * proc_queue = pqueue_create();

    while ( line_size != 0 ) {
        line_size = p1getline(0, line_buf, 100);
        if (line_size > 1) {

            Proc * proc = proc_create();

            int args_size, curword_pos, tempword_pos;
            char ** cmd;
            char templine_buffer[100];

            if ( proc == NULL ) {
                perror("Error allocating memory for a singal proc object\n");
            } else {
                pqueue_push(proc_queue, proc);
                args_size = curword_pos = tempword_pos = 0;

                p1_strncpy(templine_buffer, line_buf, 100);

                while ( (tempword_pos = p1getword(templine_buffer, tempword_pos, curword_buf)) != -1 )
                        args_size++;

                cmd = (char **) malloc( (args_size + 1) * sizeof( char * ) );

                if (cmd == NULL) {
                    perror("Error allocating memory for program and args\n");
                } else {
                    int i, nl_c_pos;
                    for (i = 0; i < args_size; i++) {
                        curword_pos = p1getword(line_buf, curword_pos, curword_buf);
                        cmd[i] = p1strdup(curword_buf);
                        nl_c_pos = p1strchr(cmd[i], '\n');
                        if ( nl_c_pos != -1) cmd[i][nl_c_pos] = '\0';
                    }
                    (*(cmd + args_size)) = NULL;
                }

                proc->args_size = args_size + 1;
                proc->cmd = cmd;
            }
            curline_index++;
        }
    }
    return proc_queue;
}


void free_mem(Proc ** procs, int arg_num) {
    int i, j;
    for ( i = 0; i < arg_num; i++ ) {
        for (j = 0; j < procs[i]->args_size; j++)
            free(procs[i]->cmd[j]);

        free(procs[i]->cmd);
        free(procs[i]);
    }
}


void proc_run( Pqueue * queue ) {
    Proc * proc = queue->head;
    while ( proc != NULL ){
        proc->pid = fork();
        if (proc->pid < 0) {
            perror("cannot fork!\n");
            exit(1);
        }

        /* child process */
        if (proc->pid == 0) {
            if ( execvp(proc->cmd[0], proc->cmd) )
                perror("excevp failed\n");

            /* if child failed terminate child */
            exit(1);
        }

        /* parent process */
        else {
            proc = proc->next;
        }
    }
}


void proc_wait( Pqueue * queue ){
    Proc * proc = queue->head;
    int status;
    while (proc != NULL) {
        while ( waitpid( proc->pid, &status, WNOHANG ) != proc->pid )
           ;
        proc = proc->next;
    }
}


void proc_array_destroy() {
    int i = 0;
    for (i = 0; i < NUM_CHILD; i++) {
        proc_destroy(*(PROC_ARRAY + i));
    }

    free(PROC_ARRAY);
}


int main(void) {
    PROC_QUEUE = read_input();
    NUM_CHILD  = PROC_QUEUE->size;

    proc_run(PROC_QUEUE);

    proc_wait(PROC_QUEUE);

    pqueue_destroy(PROC_QUEUE);
    return 0;
}