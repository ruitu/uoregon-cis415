#include <stdio.h>
#include <stdlib.h>
#include "date.h"

int main(void) {
    char * date1 = "21/01/1992";
    char * date2 = "31/21/1991";
    char * date3 = "29/12/1991";
    char * date4 = "28/12/1991";
    char * date5 = "27/12/1991";
    char * date6 = "26/12/1991";
    char * date7 = "25/12/1991";
    char * date8 = "24/12/1991";
    char * date9 = "23/12/1991";

    //char * cda[9];
    //cda = { date1, date2, dat3, date4, date5, date6, date9 };
    
   // Date * da[9];

   // size_t i;
   // for (i = 0; i < 9; i++) {
   //     Date* cur_date = date_create( *(cda + i) );
   //     da[i] = cur_date;
   // }
    Date * da = date_create(date1);    
    Date * da2 = date_create(date2);    
    printf("%d\n", da->day );    
    printf("%d\n", da2->day );    
    date_destroy(da);
    printf("%d\n", da->day );

//    printf("%d\n", date_compare(da, da2) );    

    return 0;
}
