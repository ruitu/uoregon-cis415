#ifndef _TSITERATOR_H_
#define _TSITERATOR_H_
#include <pthread.h>
typedef struct tsiterator TSIterator;

/*
 * creates an tsiterator from the supplied arguments; it is for use by the
 * tsiterator create methods in ADTs
 *
 * tsiterator assumes responsibility for elements[] if create is succesful
 * i.e. it_destroy will free the array of pointers
 *
 * returns pointer to tsiterator if successful, NULL otherwise
 */
TSIterator *tsit_create(pthread_mutex_t * lock, long size, void **elements);

/*
 * returns 1/0 if the tsiterator has a next element
 */
int tsit_hasNext(TSIterator *tsit);

/*
 * returns the next element from the tstsiterator in `*element'
 *
 * returns 1 if successful, 0 if unsuccessful (no next element)
 */
int tsit_next(TSIterator *tsit, void **element);

/*
 * destroys the tstsiterator
 */
void tsit_destroy(TSIterator *tsit);

#endif /* _ITERATOR_H_ */
