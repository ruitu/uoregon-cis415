OSC 2.18: What are the two models of interprocess communication? What are the strengths
and weaknesses of the two approaches?

The two models are shared memory and message passing
    - Shared memory:
        - Pros:
            - Faster than message passing.
			- Only when establishing memory region system calls will be required.
			- No help from the kernel is reuiqred once the shared memory has been established.
        - Cons: 
            - Shared memory suffers from cache coherency issues, because shared 
              data migrate among the several caches.

    - Message Passing:
        - Pros:
            - Useful for exchanging small amounts of data, because there is no conflict.
            - Easier to implement distributed system than shared memory 

        - Cons:
			- Slower than shared memory because it is implemented using system calls.
			- Require more time-consuming task of kernel intervention.

OSC 2.19: Why is the separation of mechanism and policy desirable?

	- The separation between those two are important because, the system can be more flexible. Policies
	  are like parameters been passed to the function, and mechanism are like the function. Which means,
	  if we implement each policy as a function, each change of a policy would result a change of the 
	  function, which is not desirable. However, if we keep them seperate, we just need to feed the 
	  mechanism with different parameters to ensure the carry out of the policies.

OSC 3.9: Describe the actions taken by a kernel to context-switch between processes?

	- When a context switch happens, the kernel store the context of the old process in its PCB, after it 
	  it is done, it loads the stored context of the new process and run it. The stored infomation includes
      CPU register, the process state, and memory-management information.

OSC 3.18: What are the benefits and the disadvantages of each of the following? Consider
both the system level and the programmer level.
    a. Synchronous and asynchronous communication

        Synchronous:
            Pros:
                -   allows a rendezvous between the sender and receiver
                -   can solve consumer and producer model problem
            Cons:
                -   Having both parties reach a rendezvous might not be needed.

        asynchronous:
            Pros:
                -   Non-blocking, can better utilize CPU time
                -   Increase the overall performance of the program since, it always do useful work
                    instead of waiting for results.                                                            
            Cons:
                -   does not  allows a rendezvous between the sender and receiver 

    b. Automatic and explicit buffering

        Automatic (Unbounded)
            Pros:
                -   Provide an unlimited buffering which means, it will never block the sender or the 
                    receiver 
            Cons:
                -   It can be a memory hog.
         explicit (Bounded )
            Pros: 
                -   Preventing it from using too much memory.
            Cons:
                -   When the queue is full sender is blocked.

     c. Send by copy and send by reference

        by copy:
            Pros:
                -   Receiver cannot change the state of the parameter.
                
        by reference:
            Pros:
                -   Receiver can change the state of the parameter.

     d. Fixed-sizes and variable-sized messages
        fixed-sized:
            Pros:
                -   Easy for OS developpers
                -   The buffer can hold a specific number of messages
            Cons:
                -   Hard for other developpers
        variable sized:
            Pros:
                -   Easy for other developpers
            Cons:
                -   Hard for OS developpers
                -   The number of message buffer can hold is a variable.

OSC 4.14: A system with two dual-core processors has four processors available for
scheduling. A CPU-intensive application is running on this system. All input is performed at
program start-up, when a single file must be opened. Similarly, all output is performed just
before the program terminates, when the program results must be written to a single file.
Between start-up and termination, the program is entirely CPU-bound. Your task is to
improve the performance of this application by multithreading it. The application runs on a
system that uses the one-to-one threading model (each user thread maps to a kernel thread).

    a. How many threads will you create to perform the input and output? Explain.
        - Reading input and writting output cannot be speeded up regardless the number of cores.
		  More threads will not help with the input and output operation, because other threads
		  have to wait until the I/O to complete. 
        
    b. How many threads will you create for the CPU-intensive portion of the application? Explain
		- If everything can be computed in parallel than, 4 would be the optimial case, because each
		  core can process a thread, however, if the application cannot be computed all in parallel,
		  then the number of thread can be used cannot be determined with the information given.

OSC 4.18: Consider a multicore system and a multithreaded program written using the manyto-many
threading model. Let the number of user-level threads in the program be greater than
the number of processing cores in the system. Discuss the performance implications of the
following scenarios.

    a. The number of kernel threads allocated to the program is less than the number of
       processing cores.
        - Some of the core are not doing useful work, because scheduler does not map user-level
          thread to process, but only the kernel thread.

    b. The number of kernel threads allocated to the program is equal to the number of
       processing cores.
        - All of the cores might do useful work the same time, but there might be situation in which
          a kernel thread got blocked inside of the kernel. Then the core which is responsible for 
          the thread does not do useful work at the mean time. 
    
    c. The number of kernel threads allocated to the program is greater than the number of
       processing cores but is still less than the number of user-level threads
        - All cores will be busy, because if a thread is blocked, then anther thread can be
          sawpped and to make the core do useful worke.

Process Analysis of sleep 600&:

    -  Ask Linux Programmer's Manual for more information
        - man proc

    - Too fully explore everything in the virtual file system better login as root
        - su 

    - Locate the process I want to explore
        - /proc/4310

    - Find out which control groups this process belongs to
        - cat cgroup
        - in this exploration I found one entry "1:name=systemd:/user.slice/user-1000.slice/session-c1.scope"
            - "1"
                - hierarchy ID number

            - name=systemd 
                - subsystems bound to the hierarchy

            - control groups the process belongs  
                - /user.slice/user-1000.slice/session-c1.scope
    
    - Find out if a process is a zombie process
        - It give me sleep600 which is the command I used to create this process
        - If the "cat cmdline" returns 0 characters if it is a zombie, which means
          it is not a zombie.

    - Find out the current working directory of process [pid]
        - readlink cwd
        - It give me /proc, so the working directory of the current process is in
          proc

    - Find out the pathname of the executed command
        - readlink exe
        - It gives me "/bin/sleep", so I know that's were its binary lays

    - Find out the files's descriptor the current process opened
        - cd fdinfo
        - ls
        - cat 0
        - pos:  0
          flags:  0100002
          mnt_id: 21
        - pos shows me the file offset, flags shows me the access mode, mnt_id, gives
          me the mouting point of the file

    - Find out the current process's resource limits
        - cat limits
        Limit                     Soft Limit           Hard Limit           Units     
        Max cpu time              unlimited            unlimited            seconds   
        Max file size             unlimited            unlimited            bytes     
        Max data size             unlimited            unlimited            bytes     
        Max stack size            8388608              unlimited            bytes     
        Max core file size        0                    unlimited            bytes     
        Max resident set          unlimited            unlimited            bytes     
        Max processes             7859                 7859                 processes 
        Max open files            1024                 65536                files     
        Max locked memory         65536                65536                bytes     
        Max address space         unlimited            unlimited            bytes     
        Max file locks            unlimited            unlimited            locks     
        Max pending signals       7859                 7859                 signals   
        Max msgqueue size         819200               819200               bytes     
        Max nice priority         0                    0                    
        Max realtime priority     0                    0                    
        Max realtime timeout      unlimited            unlimited            us    

        
    - Find out status information about a process
        - cat status
        - status S told me it is sleeping
        - voluntary_ctxt_switches and nonvoluntary_ctxt_switches told me how many times the process
          was taken off CPU.

        - cat sched
          nr_switches                                  :                    4
          nr_voluntary_switches                        :                    1
          nr_involuntary_switches                      :                    3
          gives me the total switches

    - Find out system call used by this process
        - cat syscall
        - then it gives me that 35 0x7fffc5460e50 0x0 0x0 0x16b 0x0 0x0 0x7fffc5460e48 0x7f2388c65bd0
        - The hex value must be memory address, and the first number must be system call's index.
        - grep * /usr/include 
        - find out it is #define __NR_nanosleep 35
        - find a great tool to lookup the system call by index: ausyscall

    - Find out the info of the threads in the process
        - cd task
        - ls 
        - so I find out for sleep 600& there is one thread running

