#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "p1fxns.h"
#include "proc_queue.h"

int wakeup_received = 0;
int alarm_received  = 0;

Pqueue * read_input() {
    char line_buf[100], curword_buf[100];
    int curline_index = 0, line_size = -1;

    Pqueue * proc_queue = pqueue_create();

    while ( line_size != 0 ) {
        line_size = p1getline(0, line_buf, 100);
        if (line_size > 1) {

            Proc * proc = proc_create();

            int args_size, curword_pos, tempword_pos;
            char ** cmd;
            char templine_buffer[100];

            if ( proc == NULL ) {
                perror("Error allocating memory for a singal proc object\n");
            } else {
                pqueue_push(proc_queue, proc);
                args_size = curword_pos = tempword_pos = 0;

                strncpy(templine_buffer, line_buf, 100);
                while ( (tempword_pos = p1getword(templine_buffer, tempword_pos, curword_buf)) != -1 )
                        args_size++;

                cmd = (char **) malloc( (args_size + 1) * sizeof( char * ) );

                if (cmd == NULL) {
                    perror("Error allocating memory for program and args\n");
                } else {
                    int i, nl_c_pos;
                    for (i = 0; i < args_size; i++) {
                        curword_pos = p1getword(line_buf, curword_pos, curword_buf);
                        cmd[i] = p1strdup(curword_buf);
                        nl_c_pos = p1strchr(cmd[i], '\n');
                        if ( nl_c_pos != -1) cmd[i][nl_c_pos] = '\0';
                    }
                    (*(cmd + args_size)) = NULL;
                }

                proc->args_size = args_size + 1;
                proc->cmd = cmd;
            }
            curline_index++;
        }
    }
    return proc_queue;
}


void free_mem(Proc ** procs, int arg_num) {
    int i, j;
    for ( i = 0; i < arg_num; i++ ) {
        for (j = 0; j < procs[i]->args_size; j++)
            free(procs[i]->cmd[j]);

        free(procs[i]->cmd);
        free(procs[i]);
    }
}


void wakeup_handler( int wakeup ) {
    if (wakeup == SIGUSR1) {
        wakeup_received = 1;
    }
}


void alarm_hander( int alarm ) {
    if (alarm == SIGALRM) {
        alarm_received = 1;
    }
}


int is_terminated( Proc * proc) {
    pid_t c_pid;
    int status;
    if ( (c_pid = waitpid( proc->pid, &status, WNOHANG )) == 0) {
        return 0;
    } else if (c_pid == proc->pid) {
        return 1;
    } else {
        perror("Termination check error\n");
        return -1;
    }
}


void send_all( Pqueue * queue, int signal ) {
    Proc * proc = queue->head;
    while (proc != NULL) {
        kill( proc->pid, signal );
        proc = proc->next;
    }
}


void proc_run( Pqueue * queue ) {
    Proc * proc = queue->head;
    while ( proc != NULL ){
        proc->pid = fork();
        if (proc->pid < 0) {
            perror("cannot fork!\n");
            exit(1);
        }

        /* child process */
        if (proc->pid == 0) {
            while (!wakeup_received) {
                sleep(1);
            }

            if ( execvp(proc->cmd[0], proc->cmd) )
                perror("excevp failed\n");

            /* if child failed terminate child */
            exit(1);
        }

        /* parent process */
        else {
            proc = proc->next;
        }
    }
}


void install_sig_handlers(){
    if (signal(SIGUSR1, wakeup_handler) == SIG_ERR) {
       perror("Error installing wake up signal\n");
    }

    if (signal(SIGALRM, alarm_hander) == SIG_ERR) {
        perror("Error installinf alarm signal\n");
    }

    alarm_received = 0;
}


void schedule_procs( Pqueue * queue ) {
    while (queue->size) {

        int terminated = 0;

        Proc * r_proc = queue->head;

        /* -------------start the process-------------- */
        kill(r_proc->pid, SIGCONT);
        alarm(100);

        while ( !alarm_received ) {
            /* termination check also reap terminated child processes */
            if ( is_terminated(r_proc) ) {
                terminated = 1;
                break;
            }
        }

        kill(r_proc->pid, SIGSTOP);
        /* -------------stop the process--------------- */

        if ( terminated ) pqueue_pop_destory(queue);
        else              pqueue_next_turn(queue);

        install_sig_handlers();
    }
}


int main(void) {
    Pqueue * queue = read_input();

    /* install signal */
    install_sig_handlers();

    /* fork all the child process but having them wait for start signal  */
    proc_run(queue);

    /* stop all the process */
    send_all(queue, SIGSTOP);

    /* start all the processes  */
    send_all(queue, SIGUSR1);

    /* --------------------additional code goes here----------------------   */
    schedule_procs(queue);
    /* -------------------end of additional code -------------------------  */

    /* free memory allocated */
    pqueue_destroy(queue);

    return 0;
}
