#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tldlist.h"

#define MAX(x, y) ((x) > (y) ? (x) : (y))
// http://stackoverflow.com/questions/3437404/min-and-max-in-c


struct tlditerator {
    TLDNode ** nodes;
    int cur_index;
    long max_index;
};

struct tldnode {
    char * hostname;
    int height;
   	long count;
    struct tldnode * left;
    struct tldnode * right;
};


struct tldlist {
    Date * begin_date;
    Date * end_date;
    TLDNode * root;
    long size;
    long count;
};


// OK
TLDList *tldlist_create(Date *begin, Date *end) {
	TLDList * avl = (TLDList * ) malloc( sizeof(TLDList) );
	avl->begin_date = date_duplicate(begin);
	avl->end_date   = date_duplicate(end);
	avl->size	    = 0;
	avl->count      = 0;
	avl->root       = NULL;
	if (avl == NULL) free(avl);
	return avl;
}

// Ok
void tldlist_destroy(TLDList *tld) {
	TLDIterator *it =  tldlist_iter_create(tld);
	TLDNode * n;
	while (( n = tldlist_iter_next(it))) {
		free( n->hostname );
		free( n );
	}

	tldlist_iter_destroy(it);
	free(tld->begin_date);
	free(tld->end_date);
	free(tld);
}


// Ok
static int height(TLDNode * target) {
	if (target == NULL) return 0;
	else if (target->left == NULL && target->right == NULL) return 1;
	else return MAX( height(target->left), height(target->right)) + 1;
}


static TLDNode *rightRotate(TLDNode *y) {
    TLDNode *x = y->left;
    TLDNode *T2 = x->right;

    // Perform rotation
    x->right = y;
    y->left = T2;

    // Update heights
    y->height = MAX(height(y->left), height(y->right))+1;
    x->height = MAX(height(x->left), height(x->right))+1;

    // Return new root
    return x;
}

// A utility function to left rotate subtree rooted with x
// See the diagram given above.
static TLDNode *leftRotate(TLDNode *x) {
   	TLDNode *y  = x->right;
    TLDNode *T2 = y->left;

    // Perform rotation
    y->left = x;
    x->right = T2;

    //  Update heights
    x->height = MAX(height(x->left), height(x->right))+1;
    y->height = MAX(height(y->left), height(y->right))+1;

    // Return new root
    return y;
}

// Get Balance factor of node N
static int getBalance(TLDNode *N) {
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}

// OK
TLDNode * create_tldnode(char * hostname, long count, TLDNode * left_c, TLDNode * right_c) {
	TLDNode * new_node = (TLDNode *) malloc ( sizeof(TLDNode) );
	if (new_node == NULL) {
		free(new_node);
	} else {
		new_node->hostname = strdup(hostname);
		new_node->count    = count;
		new_node->left     = left_c;
		new_node->right    = right_c;
		new_node->height   = 1;
	}

	return new_node;
}

static TLDNode* insert(TLDList * tld, TLDNode * node, char *hostname) {
    /* 1.  Perform the normal BST rotation */
    if (node == NULL) {
    	tld->size++;
        return(create_tldnode(hostname, 1, NULL, NULL));
    }


    if (strcmp(node->hostname, hostname) > 0)
		node->left  = insert(tld, node->left, hostname);
    else if (strcmp(node->hostname, hostname) < 0)
 		node->right = insert(tld, node->right, hostname);
 	else
 		node->count++;

    /* 2. Update height of this ancestor node */
    node->height = MAX(height(node->left), height(node->right)) + 1;

    /* 3. Get the balance factor of this ancestor node to check whether
       this node became unbalanced */
    int balance = getBalance(node);

    // If this node becomes unbalanced, then there are 4 cases

    // Left Left Case
    if (balance > 1 && strcmp(hostname, node->left->hostname) < 0)
        return rightRotate(node);

    // Right Right Casesss
    if (balance < -1 && strcmp(hostname, node->right->hostname) > 0)
        return leftRotate(node);

    // Left Right Case
    if (balance > 1 && strcmp(hostname, node->left->hostname) < 0) {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }

    // Right Left Case
    if (balance < -1 && strcmp(hostname, node->right->hostname) > 0) {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }

    /* return the (unchanged) node pointer */
    return node;
}


// Ok
int tldlist_add(TLDList *tld, char *hostname, Date *d) {
	int counted = 0,
		sl = strlen(hostname),
		start_i = sl - 1,
		lower_bound = (date_compare(tld->begin_date, d) <= 0) ? 1 : 0,
		upper_bound = (date_compare(tld->end_date, d) >= 0) ? 1 : 0;

		while ( start_i-- > 0  &&  *(hostname + start_i) != '.')
			;

		char parsedhn[sl - start_i];
		strncpy(parsedhn, hostname + start_i + 1, sl - start_i + 1);

	if ( lower_bound && upper_bound) {
		tld->count++;
		tld->root = insert(tld, tld->root, parsedhn);
	}

	return counted;
}

//OK
// static void print_inorder(TLDNode * root) {
// 	if (root != NULL) {
// 		print_inorder(root->left);
// 		printf("hostname: %s,  height: %d\n",root->hostname, height(root));
// 		print_inorder(root->right);
// 	}
// }

static void lineup(TLDNode * cur_node, TLDNode ** node_array ,long * cur_i ) {
	if (cur_node != NULL) {
		lineup(cur_node->left, node_array, cur_i);
		node_array[ (*cur_i)++ ] = cur_node;
		lineup(cur_node->right, node_array, cur_i);
	}
}

// OK
TLDIterator *tldlist_iter_create(TLDList *tld) {
	TLDNode ** node_array = (TLDNode **) malloc( tld->size * sizeof(TLDNode *) );
	TLDIterator * it = (TLDIterator* ) malloc( sizeof(TLDIterator));

	if (it == NULL || node_array == NULL) {
		free(node_array);
		free(it);
	} else {
		long cur_index = 0;
		it->nodes     = node_array;
		it->cur_index = cur_index;
		it->max_index = tld->size;
		lineup( tld->root, node_array, &cur_index );
	}
	return it;
}


// OK
TLDNode *tldlist_iter_next(TLDIterator *iter) {
	if (iter->cur_index == iter->max_index) return NULL;
	else return *(iter->nodes + iter->cur_index++);
}


// Ok
void tldlist_iter_destroy(TLDIterator *iter) {
	free(iter->nodes);
	iter->nodes = NULL;

	free(iter);
	iter = NULL;

}


// OK
long tldlist_count(TLDList *tld)     { return tld->count; }
char *tldnode_tldname(TLDNode *node) { return node->hostname; }
long tldnode_count(TLDNode *node)    { return node->count; }
