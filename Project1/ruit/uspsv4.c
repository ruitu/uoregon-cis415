#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "p1fxns.h"
#include "proc_queue.h"
#define TIME_SLICE 1

int wakeup_received = 0;
int STARTED = 0;
int NUM_CHILD = 0;
Pqueue * PROC_QUEUE;
Proc   * CUR_PROC;
Proc  ** PROC_ARRAY;

// http://linux.die.net/man/3/strncpy crabbed from here
static char * p1_strncpy(char *dest, const char *src, size_t n) {
    size_t i;

   for (i = 0; i < n && src[i] != '\0'; i++)
        dest[i] = src[i];
    for ( ; i < n; i++)
        dest[i] = '\0';

   return dest;
}


// http://www.sanfoundry.com/c-program-integer-to-string-vice-versa/
static void tostring(char str[], int num) {
    int i, rem, len = 0, n;
 
    n = num;

    while (n != 0) {
        len++;
        n /= 10;
    }

    for (i = 0; i < len; i++) {
        rem = num % 10;
        num = num / 10;
        str[len - (i + 1)] = rem + '0';
    }

    str[len] = '\0';
}


void print_exec_info( pid_t pid ) {
    size_t b_size = 1024;
    char line_buffer[b_size];
    char pid_string[b_size];
    char temp_time_buffer[b_size];
    char path[b_size];
    int cur_pos = 0;
    int tempword_pos = 0;

    tostring(pid_string, pid);
    p1_strncpy( (path + cur_pos), "/proc/", (p1strlen("/proc/")) );
    cur_pos += p1strlen("/proc/");
    p1_strncpy( (path + cur_pos), pid_string, p1strlen(pid_string) + 1);
    cur_pos += p1strlen(pid_string);
    p1_strncpy( (path + cur_pos), "/stat", p1strlen("/stat") + 1 );
    
    int fd = open(path, O_RDONLY);
    read(fd, line_buffer, b_size);

    char user_time[b_size], kernel_time[b_size];

    p1putstr(1, "Execution Status:\n");

    int i;
    for (i = 0; i < 52; i++) {
        tempword_pos = p1getword(line_buffer, tempword_pos, temp_time_buffer);

        if (i == 2) {
            p1putstr(1, "state:                   "); 
            p1putstr(1, temp_time_buffer);
            p1putstr(1, "\n");      
      
        }

        if (i == 13) {
            p1putstr(1, "User execution time:     "); 
            p1putstr(1, temp_time_buffer);
            p1putstr(1, "\n");
        } 

        if (i == 14) {
            p1putstr(1, "Priority execution time: "); 
            p1putstr(1, temp_time_buffer);
            p1putstr(1, "\n");
        }
    }
    close(fd);
}


void print_io_info( pid_t pid ) {
    size_t b_size = 1024;

    char line_buffer[b_size];
    char pid_string[b_size];
    char temp_word_buffer[b_size];
    char path[b_size];
    int  cur_pos = 0;

    tostring(pid_string, pid);
    p1_strncpy( (path + cur_pos), "/proc/", (p1strlen("/proc/")) );
    cur_pos += p1strlen("/proc/");
    p1_strncpy( (path + cur_pos), pid_string, p1strlen(pid_string) + 1);
    cur_pos += p1strlen(pid_string);
    p1_strncpy( (path + cur_pos), "/io", p1strlen("/io") + 1 );

    int fd = open(path, O_RDONLY);

    p1putstr(1,"I/O info: \n");
    int i, j;
    for (i = 0; i < 7; i++) {
        p1getline(fd, line_buffer, b_size);
        int  tempword_pos = 0;

        for (j = 0; j < 2; j++) {
            tempword_pos = p1getword(line_buffer, tempword_pos, temp_word_buffer);
        }

        if (i == 0) {
            p1putstr(1, "Characters read          "); 
            p1putstr(1, temp_word_buffer);
        }

        if (i == 1) {
            p1putstr(1, "Characters writen        "); 
            p1putstr(1, temp_word_buffer);
        }

        if (i == 2) {
            p1putstr(1, "Read system calls        "); 
            p1putstr(1, temp_word_buffer);
        }

        if (i == 3) {
            p1putstr(1, "Write system calls       "); 
            p1putstr(1, temp_word_buffer);
        }
    }
    close(fd);
}


void print_mem_info( pid_t pid ) {
    size_t b_size = 1024;

    char line_buffer[b_size];
    char pid_string[b_size];
    char temp_word_buffer[b_size];
    char path[b_size];
    int  cur_pos = 0;
    int  tempword_pos = 0;

    tostring(pid_string, pid);
    p1_strncpy( (path + cur_pos), "/proc/", (p1strlen("/proc/")) );
    cur_pos += p1strlen("/proc/");
    p1_strncpy( (path + cur_pos), pid_string, p1strlen(pid_string) + 1);
    cur_pos += p1strlen(pid_string);
    p1_strncpy( (path + cur_pos), "/statm", p1strlen("/statm") + 1 );

    int fd = open(path, O_RDONLY);
    p1getline(fd, line_buffer, b_size);

    p1putstr(1, "Memory info: \n");
    int i;
    for (i = 0; i < 7; i++) {
        tempword_pos = p1getword(line_buffer, tempword_pos, temp_word_buffer);

        if (i == 0) {
            p1putstr(1, "Program size:            "); 
            p1putstr(1, temp_word_buffer);
            p1putstr(1, "\n");

        }

        if (i == 2) {
            p1putstr(1, "Shared pages:            "); 
            p1putstr(1, temp_word_buffer);
            p1putstr(1, "\n");
        }

        if (i == 6) {
            p1putstr(1, "Dirty pages:             "); 
            p1putstr(1, temp_word_buffer);
        }
    }
}


Pqueue * read_input() {
    char line_buf[100], curword_buf[100];
    int curline_index = 0, line_size = -1;

    Pqueue * proc_queue = pqueue_create();

    while ( line_size != 0 ) {
        line_size = p1getline(0, line_buf, 100);
        if (line_size > 1) {

            Proc * proc = proc_create();

            int args_size, curword_pos, tempword_pos;
            char ** cmd;
            char templine_buffer[100];

            if ( proc == NULL ) {
                perror("Error allocating memory for a singal proc object\n");
            } else {
                pqueue_push(proc_queue, proc);
                args_size = curword_pos = tempword_pos = 0;

                p1_strncpy(templine_buffer, line_buf, 100);

                while ( (tempword_pos = p1getword(templine_buffer, tempword_pos, curword_buf)) != -1 )
                        args_size++;

                cmd = (char **) malloc( (args_size + 1) * sizeof( char * ) );

                if (cmd == NULL) {
                    perror("Error allocating memory for program and args\n");
                } else {
                    int i, nl_c_pos;
                    for (i = 0; i < args_size; i++) {
                        curword_pos = p1getword(line_buf, curword_pos, curword_buf);
                        cmd[i] = p1strdup(curword_buf);
                        nl_c_pos = p1strchr(cmd[i], '\n');
                        if ( nl_c_pos != -1) cmd[i][nl_c_pos] = '\0';
                    }
                    (*(cmd + args_size)) = NULL;
                }

                proc->args_size = args_size + 1;
                proc->cmd = cmd;
            }
            curline_index++;
        }
    }
    return proc_queue;
}


void free_mem(Proc ** procs, int arg_num) {
    int i, j;
    for ( i = 0; i < arg_num; i++ ) {
        for (j = 0; j < procs[i]->args_size; j++)
            free(procs[i]->cmd[j]);

        free(procs[i]->cmd);
        free(procs[i]);
    }
}


void send_all( Pqueue * queue, int signal ) {
    Proc * proc = queue->head;
    while (proc != NULL) {
        kill( proc->pid, signal );
        proc = proc->next;
    }
}


void proc_run( Pqueue * queue ) {
    Proc * proc = queue->head;
    while ( proc != NULL ){
        proc->pid = fork();
        if (proc->pid < 0) {
            perror("cannot fork!\n");
            exit(1);
        }

        /* child process */
        if (proc->pid == 0) {
            while (!wakeup_received) {
                sleep(1);
            }

            if ( execvp(proc->cmd[0], proc->cmd) )
                perror("excevp failed\n");

            /* if child failed terminate child */
            exit(1);
        }

        /* parent process */
        else {
            proc = proc->next;
        }
    }
}


void wakeup_handler( int wakeup ) {
    if (wakeup == SIGUSR1) {
        wakeup_received = 1;
    }
}


void setpid_dead( pid_t pid ) {
    int i;
    for (i = 0; i < NUM_CHILD; i++) {
        if ( PROC_ARRAY[i]->pid == pid ) {
            PROC_ARRAY[i]->dead = 1;
            break;
        }
    }
}


void sigchild_handler( int sigchld ) {
    pid_t pid;
    int status;

    while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
        if (WIFEXITED(status)) {
            setpid_dead(pid);
        }
    }
}


void alarm_hander( int alarm_sig ) {
    kill(PROC_QUEUE->head->pid, SIGSTOP);
    int popped = 0;
    while (PROC_QUEUE->head != NULL && PROC_QUEUE->head->dead) {
        pqueue_pop(PROC_QUEUE);
        popped = 1;
    }
    if ( !popped && STARTED ) pqueue_next_turn( PROC_QUEUE );
    if ( PROC_QUEUE->size ) {
        if (!PROC_QUEUE->head->dead) {
            kill( PROC_QUEUE->head->pid, SIGCONT );
            alarm(TIME_SLICE);
            p1putstr(1, "\n*********************************************\n");
            print_exec_info(PROC_QUEUE->head->pid);
            p1putstr(1, "\n");
            print_io_info(PROC_QUEUE->head->pid);
            p1putstr(1, "\n");
            print_mem_info(PROC_QUEUE->head->pid);
            p1putstr(1, "*********************************************\n\n");
        }
    }
    STARTED = 1;
}


void install_sig_handlers(){
    if (signal(SIGUSR1, wakeup_handler) == SIG_ERR) {
       perror("Error installing wake up signal\n");
    }

    struct sigaction act, act1;

    memset (&act, 0, sizeof(act));
    act.sa_handler = sigchild_handler;

    if (sigaction(SIGCHLD, &act, 0)) {
        perror ("sigaction");
    }

    memset (&act1, 0, sizeof(act1));
    act1.sa_handler = alarm_hander;

    if (sigaction(SIGALRM, &act1, 0)) {
        perror ("sigaction");
    }
}


void copy_procs( Pqueue * queue ) {
    int count = 0;
    Proc * proc = queue->head;
    PROC_ARRAY = (Proc ** ) malloc( queue->size * sizeof(Proc *));
    if (PROC_ARRAY == NULL) {
        perror("pid array allocation failed\n");
        free(PROC_ARRAY);
    } else {
        while (proc != NULL) {
            *(PROC_ARRAY + count) = proc;
            count++;
            proc = proc->next;
        }
    }
}


void proc_array_destroy() {
    int i = 0;
    for (i = 0; i < NUM_CHILD; i++) {
        proc_destroy(*(PROC_ARRAY + i));
    }

    free(PROC_ARRAY);
}


int main(void) {
    PROC_QUEUE = read_input();
    NUM_CHILD  = PROC_QUEUE->size;
    copy_procs(PROC_QUEUE);

    /* install signal */
    install_sig_handlers();

    /* fork all the child process but having them wait for start signal  */
    proc_run(PROC_QUEUE);

    send_all(PROC_QUEUE, SIGUSR1);

    send_all(PROC_QUEUE, SIGSTOP);
    
    alarm(1);

    while ( PROC_QUEUE->size ) {
        sleep(1);
    }

    pqueue_destroy(PROC_QUEUE);
    
    proc_array_destroy();
    
    return 0;
}